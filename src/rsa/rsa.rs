// Copyright 2022 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use base64;

use openssl::{
    rsa::{Padding, Rsa},
    symm::Cipher,
};

use anyhow::Result;
use std::collections::HashMap;

#[allow(dead_code)]
pub fn encrypt(data: &str, public_key: &str) -> Result<String> {
    // Encrypt with public key
    let rsa = Rsa::public_key_from_pem(public_key.as_bytes()).unwrap();
    let mut buf: Vec<u8> = vec![0; rsa.size() as usize];
    let _ = rsa.public_encrypt(data.as_bytes(), &mut buf, Padding::PKCS1)?;
    // let encrypted_str =String::from_utf8(buf).unwrap();
    // println!("Encrypted:  {:?} \n {}", buf, bytes);
    let b64_encoded_str = base64::encode(&buf);

    Ok(b64_encoded_str)
}

#[allow(dead_code)]
pub fn decrypt(b64_encoded_str: &str, private_key: &str, passphrase: &str) -> Result<String> {
    let data = base64::decode(b64_encoded_str).unwrap();

    // Decrypt with private key
    let rsa = Rsa::private_key_from_pem_passphrase(private_key.as_bytes(), passphrase.as_bytes())
        .unwrap();

    let mut buf: Vec<u8> = vec![0; rsa.size() as usize];
    let bytes = rsa.private_decrypt(&data, &mut buf, Padding::PKCS1_OAEP)?;

    let utf_decoded_str = String::from_utf8(buf[..bytes].to_vec()).unwrap();

    Ok(utf_decoded_str)
}

#[allow(dead_code)]
pub fn gen_keys(passphrase: &str) -> Result<HashMap<String, String>> {
    // let passphrase = "";

    let rsa = Rsa::generate(1024 * 2).unwrap();

    let private_key: Vec<u8> = rsa
        .private_key_to_pem_passphrase(Cipher::aes_128_cbc(), passphrase.as_bytes())
        .unwrap();
    let public_key: Vec<u8> = rsa.public_key_to_pem().unwrap();

    let mut rsa_data: HashMap<String, String> = HashMap::new();

    let public_key_str = String::from_utf8(public_key).unwrap();
    let private_key_str = String::from_utf8(private_key).unwrap();

    rsa_data.insert("public_key".into(), public_key_str);
    rsa_data.insert("private_key".into(), private_key_str);

    Ok(rsa_data)
}
