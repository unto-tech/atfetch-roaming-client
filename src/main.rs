

mod config;
mod fetch;
mod logger;
mod socket_io;
// mod rsa;

use crate::logger::print_sep;
use crate::socket_io::io_start;



fn start_io() {
  print_sep(true);
  println!("Starting....");
  // start IO
  io_start().expect("Unable to connect to Server");
}

// #[tokio::main]
fn main() {

  start_io()
}
