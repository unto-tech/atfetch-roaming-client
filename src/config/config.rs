// Copyright 2022 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use anyhow::Result;
use chrono::Utc;
use dirs;
use penguin_config::*;
use randua;

// use std::collections::HashMap;

use serde::Deserialize;
use std::error::Error;
use std::io;
use validate::rules::*;

// use serde_json::json;
// use std::{
//     fs::{read_to_string, File},
//     io::{Write},
// };


// use crate::rsa::*;

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub user: String,
    pub installed: String,
    pub run: String,
    pub server: String,
    pub max_socket_retries: u8,
    pub retry_wait_seconds: u64,
    pub ping_delay_seconds: u64,
    pub network_await_seconds: u64,
    pub max_network_retries: u8,
    pub user_agent: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RsaStruct {
    pub public_key: String,
    pub private_key: String,
}

impl PenguinConfigGenerate for Config {
    fn generate_penguin_config_file() {
        let config = Config {
            user: String::from(""),
            installed: Utc::now().to_rfc3339(),
            run: Utc::now().to_rfc3339(),
            // server: String::from("http://localhost:5436"),
            server: String::from("http://nguru.ngrok.io"),
            max_socket_retries: 20,
            max_network_retries: 20,
            retry_wait_seconds: 5,
            ping_delay_seconds: 30,
            network_await_seconds: 5,
            user_agent: randua::new().to_string(),
        };

        // save config
        save_config(&config).expect("Error Saving Config");
    }
}

impl PenguinConfig for Config {
    fn read_config() -> Self {
        let conf_file = get_config_file(true, "config".into());
        let config: Self = Deserializer::file_path(&conf_file[..]).deserialize();
        config
    }
}

fn get_config_file(check_missing: bool, file_name: &str) -> String {
    let mut path = dirs::home_dir().unwrap();

    path.push(".atfetch");
    path.push("client");

    let json_file_name: String = String::from(file_name) + ".json";
    // file_name.push_str(".json");
    path.push(json_file_name);

    // if missing generate file
    if path.exists() == false && check_missing {
        match file_name {
            "config" => Config::generate_penguin_config_file(),
            _ => println!(""),
        };
    }

    let path_str = path.to_string_lossy().to_string();

    path_str
}

#[allow(unused_assignments)]
fn get_input() {
    let mut user: String = String::new();

    loop {
        let mut input = String::new();

        // Reads the input from STDIN and places it in the String named input.
        println!("Enter Your Email Address:");
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read input.");

        // remove trailing new line
        input.pop();

        user = match email().validate(&input[..]).is_ok() {
            true => input,
            false => continue,
        };

        break;
    }

    let mut config_data = Config::read_config();

    // set user
    config_data.user = user;

    save_config(&config_data).expect("Error Saving Config");
}

fn save_config(config_data: &Config) -> Result<(), Box<dyn Error>> {
    let conf_file = get_config_file(false, "config".into());
    // save file
    Serializer::file_path(&conf_file[..]).serialize(&config_data);

    Ok(())
}

pub fn read_config() -> Config {
    let mut config = Config::read_config();

    // if missing values...
    if config.user == "" {
        get_input();
        config = read_config();
    }

    // update run time
    config.run = Utc::now().to_rfc3339();

    // save file
    save_config(&config).expect("Error Saving Config");

    config
}


// TODO: Sort encryption

/*

pub fn save_rsa(rsa_data: &HashMap<String, String>, file_name: &str) -> Result<()> {
    let rsa_file = get_config_file(false, file_name);
    let rsa_data_json = json!(rsa_data);
    let mut output = File::create(&rsa_file)?;
    write!(output, "{}", &rsa_data_json)?;
    Ok(())
}

pub fn load_rsa(file_name: &str) -> Result<HashMap<String, String>> {

    let rsa_file = get_config_file(false, file_name);

    // load file or blank object
    let rsa_data_json = read_to_string(rsa_file).unwrap_or("{}".into());
    let mut rsa_data: HashMap<String, String> = serde_json::from_str(&rsa_data_json[..]).unwrap();

    // create rsa_file if missing
    if file_name == "self_rsa"   {
        if !rsa_data.contains_key("public_key"){
            // we need to create keys
            let passphrase="";
            rsa_data = gen_keys(passphrase)?;
            // save rsa
            save_rsa(&rsa_data, file_name)?;
        }
    }

    // return rsa_data
    Ok(rsa_data)
}

*/
