// Copyright 2022 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use reqwest;

use anyhow::Result;

use serde::{Deserialize, Serialize};
use serde_json::Value;

use std::{collections::HashMap, time::Duration};

// local crates
use crate::config::*;
use crate::logger::*;

// TODO: At the moment, encryption fails for long text
// use crate::rsa::*;

#[derive(Serialize, Deserialize, Debug)]
pub struct PayloadStruct {
    pub _type: String,
    pub body: Value,
}

impl PayloadStruct {
    pub fn init() -> PayloadStruct {
        let obj = PayloadStruct {
            _type: "None".into(),
            body: serde_json::from_str("{\"val\":\"\"}").unwrap(),
        };

        obj
    }

    // convert string to payload object
    pub fn to_obj(&mut self, body_str: &str) {
        let obj: PayloadStruct = match serde_json::from_str(&body_str) {
            Ok(obj) => obj,
            _ => {
                // return blank body
                // self::init()
                PayloadStruct {
                    _type: "None".into(),
                    body: serde_json::from_str("{\"val\":\"\"}").unwrap(),
                }
            }
        };

        self._type = obj._type;
        self.body = obj.body;
    }

    // dtry to ecrypt
    pub fn try_decrypt(&mut self) -> Result<()> {
        // TODO: At the moment, encryption fails for long text
        /*
        // if is encoded, then body will have the enc key that will contain encoded data
        match self.body.get("enc") {
            // Ok so the body is indeed encoded
            Some(encrypted_val) => {
                // load encryption keys
                let self_rsa = load_rsa("self_rsa")?;
                let private_key = self_rsa.get("private_key".into()).unwrap();
                let encrypted_str = encrypted_val.as_str().unwrap();

                let decrypted_data = decrypt(&encrypted_str, &private_key, "") ;


                match decrypted_data {
                    Ok(mut decoded_str) => {
                        // we have decoded a string
                        decoded_str = format!(
                            "{{\n\"_type\":\"fetch-encoded-request\",\n \"body\": {} \n}}",
                            decoded_str
                        );

                        // add body and the rest
                        // println!("{}", decoded_str);

                        // update self
                        let mut obj: PayloadStruct = PayloadStruct::init();
                        obj.to_obj(&decoded_str);

                        // println!("Received: {:#?}", obj);

                        // update self
                        self._type = obj._type;
                        self.body = obj.body;
                    }
                    Err(e) => println!("\n\n ENC1: {:#?}", e),
                };
            }
            _ => (),
        };*/

        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Resp {
    req_id: Option<String>,
    _type: Option<String>,
    url: Option<String>,
    body: Option<String>,
    status: Option<(u16, String)>,
    headers: Option<HashMap<String, String>>,
}

#[allow(dead_code)]
impl Resp {
    fn blank() -> Resp {
        Resp {
            req_id: None,
            _type: None,
            url: None,
            body: None,
            status: None,
            headers: None,
        }
    }

    fn encrypt(&mut self) -> Result<()> {
        // TODO: At the moment, encryption fails for long text
        /*
        // if server sent encrypted data, the we respond in a similar fashion
        let _type = &self._type;
        let enc_type = "fetch-encoded-request";

        let is_server_encrypted: bool = match _type {
            Some(e) => e == enc_type,
            _ => false,
        };

        if is_server_encrypted {
            println!("Attempting Encryption...");
            // first convert to json
            let body = json!(self);
            let body_str = &body.to_string();
            let self_rsa = load_rsa("self_rsa")?;
            let public_key = self_rsa.get("public_key".into()).unwrap();

            let text ="To ensure that the cell is initialized only once in all its lifetime, if try_init_once or init_once are called reentranly from the f argument they take, the result of that reentrant call will be used and the return value of their caller f will be ignored.";

            TODO: At the moment, encryption fails for long text
            let enc = encrypt(text, &public_key);
            println!("{:?}\n\n{:?}", &body_str, enc);
        }
        */

        Ok(())
    }
}

// Noop serde_json::Value
#[derive(Serialize, Deserialize, Debug)]
struct Noop {
    val: Value,
}

#[allow(dead_code)]
impl Noop {
    fn as_str() -> Value {
        let noop_data: Noop = serde_json::from_str("{\"val\":\"\"}").unwrap();
        noop_data.val
    }
    fn as_obj() -> Value {
        let noop_data: Noop = serde_json::from_str("{\"val\":{}}").unwrap();
        noop_data.val
    }
    fn as_num() -> Value {
        let noop_data: Noop = serde_json::from_str("{\"val\":0}").unwrap();
        noop_data.val
    }
}

#[allow(dead_code)]
fn type_of<T>(_: &T) -> String {
    format!("{}", std::any::type_name::<T>())
}

// Function to get body key values
// All values returned as strings so parse as needed : let parsed: i32 = val.parse().unwrap();
pub fn get_body_value(payload_struct: &PayloadStruct, key_name: &str) -> String {
    // Get Value or assume is empty string
    let noop_data = Noop::as_str();
    let data: &serde_json::Value = payload_struct.body.get(key_name).unwrap_or(&noop_data);

    let string: String = match data.as_str() {
        Some(s) => s.into(),
        _ => "".into(),
    };

    string
}

fn make_client() -> reqwest::Client {
    let mut headers = reqwest::header::HeaderMap::new();

    // random user agent
    let config_data = read_config();
    let ua = config_data.user_agent;
    headers.insert(
        reqwest::header::USER_AGENT,
        reqwest::header::HeaderValue::from_str(&ua[..]).unwrap(),
    );

    // headers.insert(reqwest::header::CONTENT_LENGTH, "4343434".parse().unwrap());

    let client = reqwest::Client::builder()
        .default_headers(headers)
        .timeout(Duration::from_secs(10))
        .build()
        .expect("Unable To Make Client1");

    client
}

#[tokio::main]
async fn fetch(payload_struct: &mut PayloadStruct) -> Result<Resp> {
    // attempt to decrypt payload if encrypted
    payload_struct.try_decrypt()?;

    // println!("SSSS{:#?}", payload_struct);

    // get Data or return Value as empty object
    let noop_data = Noop::as_str();
    // Ok Now let us read the payload
    let _type = &payload_struct._type;

    let req_id = payload_struct
        .body
        .get("req_id")
        .unwrap_or(&noop_data)
        .as_str()
        .unwrap();
    let url = get_body_value(payload_struct, "url");
    let method = get_body_value(payload_struct, "method");
    let mut submit_as = get_body_value(payload_struct, "as");

    // get Data or return Value as empty object
    let noop_data = Noop::as_obj();
    let data: &serde_json::Value = payload_struct.body.get("data").unwrap_or(&noop_data);

    // if Data is String
    let data_is_string: bool = match &data.as_str() {
        Some(_) => true,
        _ => false,
    };

    // if Data is Object
    let data_is_object: bool = match &data.as_object() {
        Some(_) => true,
        _ => false,
    };

    let mut data_to_submit = HashMap::new();

    // if data is string always submit with body
    if data_is_string {
        submit_as = "body".into();
    } else if data_is_object {
        let data_object = match data.as_object() {
            Some(o) => o,
            _ => panic!("Ain't special"),
        };

        // make submission data
        for (k, v) in data_object {
            // println!("k{}, v{}", k, v)
            // insert data into Hashmap
            data_to_submit.insert(k, v);
        }
    }

    let client = make_client();

    // set the correct menthod
    let client: reqwest::RequestBuilder = match &method[..] {
        "post" => client.post(&url),
        "put" => client.put(&url),
        "delete" => client.delete(&url),
        _ => client.get(&url),
    };

    // how do we submit form
    let client: reqwest::RequestBuilder = match &submit_as[..] {
        "body" => {
            // for body always send string
            let body_str = get_body_value(payload_struct, "data");
            client.body(body_str)
        }
        "form" => client.form(&data_to_submit),
        _ => client.json(&data_to_submit),
    };

    print_sep(false);
    println!("Fetching {} ...", url);

    let res = client.send().await?;

    let headers = res.headers().clone();
    let status = res.status().clone();
    let body = res.text().await?;

    // make headers object
    let mut headers_as_obj: HashMap<String, String> = HashMap::new();

    for (k, v) in headers.iter() {
        let header_val: String = String::from_utf8(v.as_bytes().to_vec()).unwrap_or("XXXX".into());
        let header_name: String = String::from(k.as_str());
        headers_as_obj.insert(header_name, header_val);
    }

    let status_as_vec = (
        status.as_u16(),
        String::from(status.canonical_reason().unwrap()),
    );

    let resp = Resp {
        req_id: Some(req_id.to_string()),
        _type: Some(_type.to_string()),
        url: Some(url),
        body: Some(body),
        status: Some(status_as_vec),
        headers: Some(headers_as_obj),
    };

    Ok(resp)
}
pub fn get_url(payload_struct: &mut PayloadStruct) -> Result<Resp> {
    //
    // println!("{:#?}", payload_struct.body);

    let mutable_struct = payload_struct;

    let mut resp: Resp = match fetch(mutable_struct) {
        Ok(r) => r,
        _ => Resp::blank(),
    };

    // Encrypt if need be
    resp.encrypt()?;

    // println!("Body:\n\n{}", json_str);

    Ok(resp)
}
