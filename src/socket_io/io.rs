// Copyright 2022 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use machine_uid;

use anyhow::Result;
use online::sync::check;
use rust_socketio::{Client, ClientBuilder, Payload};
use serde::{Deserialize, Serialize};
use serde_json::json;

use std::{error::Error, process, thread::sleep, time::Duration};

use crate::config::*;
use crate::fetch::*;
use crate::logger::*;
// use crate::rsa::*;

// mutex data
#[derive(Debug, Serialize, Deserialize)]
struct IpResp {
    origin: String,
}

fn _print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}

fn reg_client(config_data: &Config, socket: &Client) -> Result<(), Box<dyn Error>> {
    // load RSA KEYS
    // let self_rsa = load_rsa("self_rsa")?;

    // let httpbin_resp = get_text("https://www.httpbin.org/ip")?;
    // let ip : IpResp = serde_json::from_str(&httpbin_resp[..]).unwrap();
    let ip = IpResp {
        origin: "196.216.84.205".into(),
    };

    let mac_id: String = machine_uid::get().unwrap();
    let user = &config_data.user;

    // let public_key = match self_rsa.get("public_key") {
    //     Some(s) => s,
    //     _ => "Error!",
    // };

    let client_data = json!({
        "type" : "reg-client",
        "client" : {
            "network" : {
                "ip":ip.origin
            },
            "mac":mac_id,
            "user" : user,
            // "public_key" : public_key
        }
    });

    // println!("mac:{} ip:{:?} user:{}", mac_id, ip.origin, user);

    socket.emit("data", client_data)?;

    Ok(())
}

fn ping(config_data: &Config, socket: &Client) {
    loop {
        let client_data = json!({
            "type": "ping"
        });

        let p = socket.emit("data", client_data);
        let Config {
            ping_delay_seconds, ..
        } = config_data;

        match p {
            Ok(_) => {
                // pause between pings
                sleep(Duration::from_secs(*ping_delay_seconds));
                // continue with loop
                continue;
            }
            Err(error) => {
                // Error!!!!
                println!("Error Pinging Server: {:#?}", error);
                // restart connections
                io(&config_data, 0).expect("Unable to Reconnect after Ping");
            }
        };
    }

    // Ok(())
}

#[allow(unused_assignments)]
fn io(config_data: &Config, retry_count: u8) -> Result<(), Box<dyn Error>> {
    // Get Values by Destructuring
    let Config {
        max_socket_retries,
        retry_wait_seconds,
        network_await_seconds,
        max_network_retries,
        server,
        ..
    } = config_data;

    // let server = "https://nguru.ngrok.io";
    // let server = "http://localhost:5436";

    let mut network_retries: u8 = 0;

    print_sep(true);
    println!("Connecting to {} [{} retries]...", server, retry_count);

    let callback = |payload: Payload, socket: Client| {
        match payload {
            Payload::String(value) => {
                // println!("{}", retry_count);
                // println!("Received: {:#?}", value);
                // let v = serde_json::from_str(&r[..]);
                let payload_struct: &mut PayloadStruct = &mut PayloadStruct::init();
                payload_struct.to_obj(&value[..]);

                //Handle various types
                match &payload_struct._type[..] {
                    "server-reg" => {
                        // When we recieve request to register server

                        // TODO: Sort encryption

                        /*

                        // get public_key from payload
                        let public_key: String =
                            fetch::get_body_value(&payload_struct, "public_key");

                        // create data object
                        let mut rsa_data: HashMap<String, String> = HashMap::new();
                        // insert data
                        rsa_data.insert("public_key".into(), public_key);
                        // save rsa file
                        config::save_rsa(&rsa_data, "server_rsa").expect("Error Saving Server RSA");


                        */
                    }
                    // if fetch
                    "fetch-request" => {
                        // println!("{}", pub_key);
                        // println!("{:?}", payload_struct);
                        // pass server key during fetch

                        // ensure is not busy
                        let fetch_resp = get_url(payload_struct).expect("Actions Failed!");

                        let client_data = json!({
                            "type" : "fetch-response",
                            "resp": fetch_resp
                        });

                        // println!("{:#?}", fetch_resp);

                        socket
                            .emit("data", client_data)
                            .expect("Failed Sending Client Response");

                    }
                    "suicide" => {
                        println!("Killing duplicate client...");
                        process::exit(1)
                    }
                    // Any Other Types
                    _ => println!("Unhandled Type {}", payload_struct._type),
                };
            }
            _ => {
                // let v: Value = serde_json::from_str(&value);
                println!("Received");
            }
        }
    };

    let conn = ClientBuilder::new(server)
        .on("data", callback)
        .on("error", |err, _| println!("Error: {:?}", err))
        .connect();

    match conn {
        Ok(socket) => {
            println!("Connected!");
            // wait a little before sending first message
            // allows for channel upgrades and so on
            sleep(Duration::from_secs(2));
            // register client
            reg_client(&config_data, &socket)?;
            // start pining
            ping(&config_data, &socket);
        }
        Err(error) => {
            // We have an error! check internet connection
            loop {
                match check(None).is_ok() {
                    true => break,
                    false => {
                        println!(
                            "Awaiting Internet Connection... [{} retries]",
                            network_retries
                        );
                        network_retries = network_retries + 1;

                        sleep(Duration::from_secs(*network_await_seconds));

                        // check whether we have retried enough
                        match &network_retries > max_network_retries {
                            true => {
                                println!(
                                    "Failed to get Internet Connection after {} retries",
                                    network_retries
                                );
                                process::exit(1)
                            }
                            false => continue,
                        }
                    }
                }
            }

            // if has retried too many times
            if &retry_count >= max_socket_retries {
                println!(
                    "Unable to connect to {} after {} retries",
                    server, retry_count
                );
                process::exit(1);
            }
            println!("Error Connecting to Server: {}", error);
            // wait a bit
            sleep(Duration::from_secs(*retry_wait_seconds));
            // try reconnect
            io(&config_data, retry_count + 1)?;
        }
    };

    Ok(())
}

pub fn io_start() -> Result<(), Box<dyn Error>> {
    // first get config Data
    let config_data = read_config();

    let resp = io(&config_data, 0);

    resp
}
